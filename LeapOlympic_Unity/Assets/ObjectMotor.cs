﻿using UnityEngine;
using System.Collections;

public class ObjectMotor : MonoBehaviour {

	private float maxSpeed = 1.0f;
	private float currentSpeed = 0.0f;
	private float disSpeed = 0.001f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(currentSpeed > 0) {
			transform.Translate(new Vector3(0, 0, currentSpeed));
			currentSpeed = currentSpeed - disSpeed;
			if(currentSpeed < 0) {
				currentSpeed = 0;
			}
		}
	}

	public void increaseSpeed() {
		currentSpeed += 0.1f;
	}
}
