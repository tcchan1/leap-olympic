﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	//varibable for controlling player character movement
	private float speed;
	private float startMoveTimer, journeyLength;
	private bool isMoving;
	private Animator anim;
	//variable for setting next target
	private ArrayList target = new ArrayList();
	private Vector3 nextTargetPosition;
	private Vector3 originalPos;

	void Start(){
		anim = GetComponent<Animator>();
		isMoving = false;
		speed = 0.05f;
		target.Add (new Vector3 (transform.position.x + 2, 0.846964f, transform.position.z));
		target.Add (new Vector3 (transform.position.x + 4, 0.846964f, transform.position.z));
		originalPos = transform.position;
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("space"))
			goToNextPoint(0);
		if(Input.GetKeyDown("a"))
			goToNextPoint(1);
		if (isMoving)
			Move ();
	}
	void goToNextPoint(int point){
		startMoveTimer = Time.time;
		journeyLength = Vector3.Distance(transform.position, (Vector3)target[point]);
		isMoving = true;
		nextTargetPosition = (Vector3)target [point];
	}
	void Move(){
		float distCovered = (Time.time - startMoveTimer) * speed;
		float fracJourney = distCovered / journeyLength;
		transform.position = Vector3.Lerp(transform.position, nextTargetPosition, fracJourney);
		anim.SetBool ("Move", true);
		if (transform.position.x > (nextTargetPosition.x-speed/2) && transform.position.x < (nextTargetPosition.x+speed/2) &&
		    transform.position.y > (nextTargetPosition.y-speed/2) && transform.position.y < (nextTargetPosition.y+speed/2) ) {
			Debug.Log("inposition");
			transform.position = nextTargetPosition;
			anim.SetBool ("Move", false);
			isMoving = false;
		}
	}
	void reset(){
		transform.position = originalPos;
	}

}
