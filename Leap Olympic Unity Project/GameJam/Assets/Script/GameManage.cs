﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManage : MonoBehaviour {

	public GameObject scoreLabel;
	public int score;

	// Question Panel
	public GameObject questionLabel;
	public GameObject optionBtnOne;
	public GameObject optionBtnTwo;
	private Text btnOneText;
	private Text btnTwoText;
	private Text questionText;

	// Used to get question & ans
	public int questionNo;
	private string question;
	private string[] ans;

	// Next Point
	private Vector2 nextPoint;

	private Text scoreText;

	// Set the sound effect list
	public AudioClip[] audios;
	// Use this for initialization
	void Start () {
		score = 0;
		questionNo = 0;
		scoreText = scoreLabel.GetComponent<Text> ();
		btnOneText = optionBtnOne.GetComponent<Text> ();
		btnTwoText = optionBtnTwo.GetComponent<Text> ();
		questionText = questionLabel.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score: " + score;
	}

	public void PlayClickEffect() {
		AudioSource.PlayClipAtPoint (audios [0], new Vector3 (0, 0, 0));
	}

	public void AddScore() {
		score++;
	}

	public void DesScore() {
		score--;
	}

	public void NextQuestion(int ansID) {
		//questionNo++;
		Data.GetNextPointAndQuestion (questionNo, ansID, ref nextPoint, ref questionNo);
		Debug.Log ("Question No.: " + questionNo + ", Next Point Pos: (" + nextPoint.x + ", " + nextPoint.y + "), Next Question No.: " + questionNo);
	}
	
	public void SetQuestionAndAns() {

		// Check the current question number > Total Question Number?
		if(questionNo < Data.GetTotalQuestion()) {
			// Get Question and Ans from the Data
			Data.GetQuestionAndAnswer(questionNo, ref question, ref ans);
		} else {
			// if the current question number is bigger than total question number, set current question number to ZERO
			questionNo = 0;
			Data.GetQuestionAndAnswer(questionNo, ref question, ref ans);
		}
		Debug.Log ("Question No.: "+questionNo+", Qustion: "+question+", Ans: "+ans);
		for(int i = 0 ; i  < ans.Length ; i++) {
			Debug.Log ("Ans: "+ans[i]);
		}

		// Set the question text and ans text to the UI Component
		questionText.text = question;
		btnOneText.text = ans [0];
		btnTwoText.text = ans [1];
	}
}
