﻿using UnityEngine;
using System.Collections;

public class UIBtnScript : MonoBehaviour {

	public GameObject questPlane;
	public Transform startingPoint;
	public Transform endingPoint;

	private bool isClicked;

	// Use this for initialization
	void Start () {
		isClicked = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick() {
		if(!isClicked) {
			questPlane.transform.position = endingPoint.position;
			isClicked = false;
		} else {
			questPlane.transform.position = startingPoint.position;
			isClicked = true;
		}
	}
}