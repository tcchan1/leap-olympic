﻿using UnityEngine;
using System.Collections;

public class Data
{
    private static string[][] questionsAndAnswers = new string[][]
    {
        new string[] { "What do we do now?", "Start Game", "End Game" },
        new string[] { "Two ways to go, what do we do now?", "Go left", "Go Right" },
        new string[] { "There is a help call from down side, what do we do now?", "Walk away", "Go down to help" },
        new string[] { "You see a person floating in the river, what do we do now?", "Call the police", "Walk away" },
        new string[] { "You see a person floating in the river, what do we do now?", "Go down to help", "Run away as a dog" }
    };

    private static int[][] nextPointsAndQuestions = new int[][]
    {
        new int[] { 0, 0, 2425, 965, 1 },
        new int[] { 0, 1, -1, -1, 1 },
        new int[] { 1, 0, -1, -1, -1 },
        new int[] { 1, 1, 4161, 965, 2 },
        new int[] { 2, 0, 4429, 593, 3 },
        new int[] { 2, 1, 4833, 1761, 4 },
        new int[] { 3, 0, -1, -1, -1 },
        new int[] { 3, 1, -1, -1, -1 },
        new int[] { 4, 0, -1, -1, -1 },
        new int[] { 4, 1, -1, -1, -1 }
    };

    public static void GetQuestionAndAnswer(int questionNo, ref string question, ref string[] answers)
    {
        question = questionsAndAnswers[questionNo][0];
        answers = new string[questionsAndAnswers[questionNo].Length - 1];
		for (int i = 0; i < questionsAndAnswers[questionNo].Length - 1; i++)
        {
			//throw new UnityException("ans"+answers[i]);
            answers[i] = questionsAndAnswers[questionNo][i+1];
        }
    }

    public static void GetNextPointAndQuestion(int questionNo, int answerNo, ref Vector2 nextPoint, ref int nextQuestionNo)
    {
        for (int i = 0; i < nextPointsAndQuestions.Length; i++)
        {
            if (nextPointsAndQuestions[i][0] == questionNo && nextPointsAndQuestions[i][1] == answerNo)
            {
                nextPoint = new Vector2(nextPointsAndQuestions[i][2] / 100.0f, nextPointsAndQuestions[i][3] / 100.0f);
                nextQuestionNo = nextPointsAndQuestions[i][4];
                break;
            }
        }
    }
	public static int GetTotalQuestion() {
		return questionsAndAnswers.Length;
	}
}