﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManagerScript : MonoBehaviour {
	public GameObject player;
	public GameObject startingPoint;
	public GameObject endingPoint;

	public bool selectLevelState;
	public bool readyState;
	public bool gameStart;
	public bool gamePlaying;
	public bool gameEnd;

	// Player Motor Script
	private ObjectMotor_run runMotorScript;

	// Used Time Counter
	public float usedTime;
	// Used to show the time
	[SerializeField] Text EndRecordText;
	[SerializeField] GameObject RecordLbl;
	[SerializeField] GameObject BestRecordLbl;
	[SerializeField] Text bestRecordText;
	[SerializeField] Text playerName;
	[SerializeField] GameObject playerNameObj;
	[SerializeField] GameObject btnSaveRecord;

	// Game NGUI
	public GameObject EndPanel;
	public GameObject LevelPanel;
	public GameObject RemindDisText;
	public GameObject CurrentSpeedText;
	[SerializeField] Text hud1;
	public GameObject LeapCursor;

	// start count down
	public bool startCountDown;

	public Material[] mat_readyPlane;
	public GameObject readyPlane;

	private Vector3 playerInitPosition;
	private int counttime;

	//gameEnd or game over or game won
	[SerializeField] GameObject MenuPanel;

	[SerializeField] GameObject btnReturn;
	[SerializeField] GameObject btnRestart;
	[SerializeField] GameObject btnNextLv;

	//variable for arcade mode
	[SerializeField] float timeLimit;
	int gameLevel = 0;
	bool playerWon = false;
	float finishTime;

	[SerializeField] RepeatFloor[] floor;

	//database variables
	string getScoreURL = "http://badeggstudio.com/getScore.php";
	string addScoreURL = "http://badeggstudio.com/addScore.php";
	
	void Awake() {
		runMotorScript = player.GetComponent<ObjectMotor_run> ();
	}

	// Use this for initialization
	void Start () {
		InitGame ();
		gameLevel = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(readyState) {
			CancelInvoke();
			readyPlane.SetActive(false);
			runMotorScript.increaseSpeed();
		}
		/*if(Input.GetKeyDown(KeyCode.R)) {
			InitGame();
		}*/
		if(gameEnd) {
			Debug.Log("GameEnd La!");
			//InitGame();
			ReloadScene();
		}
		if(!gameEnd && gameStart){
			usedTime += Time.deltaTime;
				//bestRecordText.text = GenerateMSTime(usedTime);
			// Set the Remind Distance Text (NGUI)
			timeLimit = timeLimit - usedTime/1000;
			RemindDisText.GetComponent<Text>().text = "Remind Distance: " + GetRemindDis() + " Meter";
			// Set the Current Speed Text (NGUI)
			CurrentSpeedText.GetComponent<Text>().text = "Current Speed: " + Mathf.FloorToInt(runMotorScript.GetCurrentSpeed() )+ " Meter/Second";
			if(gameLevel != 0 && !playerWon){
				if(timeLimit > 0 )
					hud1.text = "Remaining Time " + timeLimit.ToString("F1")+ " s";
				else 
					PlayerLose();
			}
			else
				hud1.text = "Time " + usedTime.ToString("F1")+ " s";
		}
	}
	public void addRecord(){
		StartCoroutine (addPlayerRecord ());
	}

	IEnumerator addPlayerRecord(){
		string addScoreURL2 = addScoreURL + "?score="+finishTime+"&name="+playerName.text;
		WWW addScoreReader = new WWW (addScoreURL2);
		yield return addScoreReader;
		if(addScoreReader.error!= null)
			bestRecordText.text = "Error saving result";
		else{
			Debug.Log(addScoreReader.text);
			StartCoroutine(showRecord());
		}
	}
	public void playerReachEndPoint(){
		MenuPanel.SetActive(true);
		ShowCursor();
		if (gameLevel != 0)
			PlayerWin ();
		else{
			StartCoroutine(showRecord());
			EndRecordText.gameObject.SetActive(true);
			RecordLbl.SetActive(true);
			bestRecordText.gameObject.SetActive(true);
			playerNameObj.SetActive(true);
			btnSaveRecord.SetActive(true);
			BestRecordLbl.SetActive(true);
			EndRecordText.text = GetRecord();
		}
	}

	IEnumerator showRecord(){
		WWW getScoreReader = new WWW (getScoreURL);
		yield return getScoreReader;
		if(getScoreReader.error!= null)
			bestRecordText.text = "Error loading result";
		else{
			bestRecordText.text = getScoreReader.text;
		}
	}
	void PlayerLose(){
		MenuPanel.SetActive(true);
		ShowCursor();
		EndRecordText.gameObject.SetActive(false);
		RecordLbl.SetActive(false);
		bestRecordText.gameObject.SetActive(false);
		BestRecordLbl.SetActive(false);
		btnNextLv.SetActive (false);
		playerNameObj.SetActive(false);
		btnSaveRecord.SetActive (false);
		btnRestart.SetActive (true);
		btnReturn.SetActive (true);
	}
	void PlayerWin(){
		EndRecordText.gameObject.SetActive(false);
		RecordLbl.SetActive(true);
		bestRecordText.gameObject.SetActive(false);
		BestRecordLbl.SetActive(false);
		playerNameObj.SetActive(false);
		btnSaveRecord.SetActive (false);
		RecordLbl.GetComponent<Text>().text = "Congratulations";
		btnNextLv.SetActive (true);
		btnRestart.SetActive (true);
		btnReturn.SetActive (true);
		playerWon = true;
	}
	public void restartLevel(){
		InitGame ();
	}
	public void nextLevel(){
		if(gameLevel< 3)
			gameLevel++;
		InitGame ();
	}
	public bool getReadyState(){
		return readyState;
	}
	public void setReadyState(bool state) {
		this.readyState = state;
	}

	public bool isGameEnd(){
		return gameEnd;
	}
	public void setGameEnd(bool state) {
		this.gameEnd = state;
	}

	public bool isGameStart() {
		return gameStart;
	}
	public void setGameStart(bool state) {
		this.gameStart = state;
	}

	public void SetLevelSelectState(bool state){
		this.selectLevelState = state;
	}

	public bool getGamePlaying() {
		return gamePlaying;
	}
	public void setGamePlaying(bool state) {
		this.gamePlaying = state;
	}

	public void SetPlayerInitPos(Vector3 pos) {
		this.playerInitPosition = pos;
	}

	public bool GetIsCountDown() {
		return startCountDown;
	}
	public void SetIsCountDown(bool state){
		this.startCountDown = state;
	}
	public void ResetCountDown() {
		this.counttime = 0;
	}

	public void InitGame() {
		usedTime = 0f;
		selectLevelState = true;
		readyState = false;
		gameEnd = false;
		gameStart = false;
		gamePlaying = false;
		startCountDown = false;
		counttime = 0;
		runMotorScript.initSpeed ();
		readyPlane.SetActive(true);
		InvokeRepeating ("CountDown", 1, 1);
		player.transform.position = startingPoint.transform.position;
		// deactive menu panel
		LevelPanel.SetActive (true);
		EndPanel.SetActive (false);
		foreach(RepeatFloor flr in floor)
			flr.resetPos();
		readyPlane.SetActive(true);
		RecordLbl.GetComponent<Text>().text = "Your Record:";
		playerWon = false;
	}

	void CountDown() {
		if(startCountDown && !selectLevelState) {
			if (counttime >= 4) {
				readyState = true;
			}
			switch (counttime) {
			case 0:
				readyPlane.renderer.material = mat_readyPlane[0];
				break;
			case 1:
				readyPlane.renderer.material = mat_readyPlane[0];
				break;
			case 2:
				readyPlane.renderer.material = mat_readyPlane[1];
				break;
			case 3:
				readyPlane.renderer.material = mat_readyPlane[3];
				break;
			}
			counttime ++;
		} else {
			//readyPlane.renderer.material = mat_readyPlane[4];
		}
	}

	string GenerateMSTime(float time) {
		float minutes = Mathf.Floor (time / 60);
		float seconds = Mathf.RoundToInt (time % 60);
		return minutes+":"+seconds;
	}
	public void setMode(int i){
		if(i == 1)
			setArcadeMode();
		else if(i == 2)
			setTimeChallengeMode();
		else 
			setFreeMode();
	}
	void setArcadeMode(){
		switch(gameLevel){
			case 1:		
						readyPlane.renderer.material = mat_readyPlane[7];
						timeLimit = 50.0f;
						break;
			case 2:
						readyPlane.renderer.material = mat_readyPlane[8];
						timeLimit = 40.0f;
						break;
			case 3:
						readyPlane.renderer.material = mat_readyPlane[9];
						timeLimit = 30.0f;
						break;
		}
		endingPoint.transform.position = new Vector3 (endingPoint.transform.position.x, endingPoint.transform.position.y,player.transform.position.z + 500);
	}
	void setTimeChallengeMode(){
		gameLevel = 0;
		readyPlane.renderer.material = mat_readyPlane[5];
		endingPoint.transform.position = new Vector3 (endingPoint.transform.position.x, endingPoint.transform.position.y,player.transform.position.z + 1000);
	}
	void setFreeMode(){
		gameLevel = 0;
		readyPlane.renderer.material = mat_readyPlane[6];
		endingPoint.SetActive (false);
	}
	int GetRemindDis() {
		Vector3 playerPos, endPos, dir;
		playerPos = player.transform.position;
		endPos = endingPoint.transform.position;
		dir = (endPos - playerPos) / Vector3.Distance (playerPos, endPos);
		int result;
		if(dir.z > 0) {
			result = ((int)Vector3.Distance (playerPos, endPos) - 2) * 1;
		} else {
			result = ((int)Vector3.Distance (playerPos, endPos) - 2) * -1;
		}

		if(result < 0) {
			result = 0;
		}
		return result;
	}
	public string GetRecord() {
		//return GenerateMSTime (usedTime);
		finishTime = usedTime;
		return usedTime.ToString ("F1");
	}

	public void ReloadScene() {
		Application.LoadLevel ("Running");
	}
	public void HideLevelPanel() {
		LevelPanel.SetActive (false);
	}
	public void ShowLevelPanel() {
		LevelPanel.SetActive (true);
	}
	public void ShowCursor() {
		LeapCursor.SetActive (true);
	}
	public void HideCursor() {
		LeapCursor.SetActive (false);
	}
}
