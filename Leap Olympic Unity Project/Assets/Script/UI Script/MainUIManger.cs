﻿using UnityEngine;
using System.Collections;

public class MainUIManger : MonoBehaviour {

	public GameObject StartPanel;
	public GameObject SelectStagePanel;

	// Use this for initialization
	void Start () {
		StartPanel.SetActive (true);
		SelectStagePanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void EndGame() {
		Application.Quit ();
	}

	public void BackToMenu() {
		StartPanel.SetActive (true);
		SelectStagePanel.SetActive (false);
	}

	public void GoToSelectGame(){
		StartPanel.SetActive (false);
		SelectStagePanel.SetActive (true);
	}

	public void GoToScene(string sceneName) {
		Application.LoadLevel (sceneName);
	}
}
