﻿using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour {

	GameManagerScript script;
	ObjectMotor_run motor;
	public float leftTranslate = 1f, rightTranslate = -1f;
	public Transform leftHand, rightHand;
	public Transform playerLeftHand, playerRightHand;
	public float upperPoint = 2.4f, lowerPoint = -0.24f;
	public float marginAllowed = 1.0f;
	private float delayer = 1.0f, checkCorrectTimer1 = 0, checkCorrectTimer2 = 0;// make sure dont double check within certain time
	public int[] SpeedtoIncreaseDifficulty;
	// Here are the booleans which are used to check the distance between player's hands and fake hands when the fake hands reach the lower / higher point.
	private bool left_expected;
	private bool right_expected;
	float speedChangeInterval, speedChangeTimer, speedChangeVary;
	void Start () {
		script = GetComponent<GameManagerScript> ();
		motor = Camera.main.gameObject.GetComponent<ObjectMotor_run> ();
		speedChangeTimer = 0;
	}
	void Update () {
		//on detection of 2 hand
		if (script.gameStart) {
			float speedMultiplier = 0.0f;
			increaseDifficulty();
			//move the left hand model
			if(leftHand.position.y > lowerPoint+ marginAllowed && leftHand.position.y < upperPoint - marginAllowed ){
				if(leftHand.transform.localScale.x > 0.07f){
					leftHand.transform.localScale -= new Vector3(0.01f,0.01f,0);
					rightHand.transform.localScale -= new Vector3(0.01f,0.01f,0);
				}
			}
			else {
				if(leftHand.transform.localScale.x < 0.1f){
					leftHand.transform.localScale += new Vector3(0.01f,0.01f,0);
					rightHand.transform.localScale += new Vector3(0.01f,0.01f,0);
				}
				if(Time.time - checkCorrectTimer1 > delayer ){
					if(checkedGestureAccuracyLeft(leftHand,playerLeftHand))
						speedMultiplier += 1f;
					checkCorrectTimer1 = Time.time;
				}
			}
			if(leftHand.position.y < lowerPoint || leftHand.position.y > upperPoint ){
				leftTranslate *= -1f;
				rightTranslate *= -1f;
				if(leftHand.position.y < lowerPoint){
					leftHand.position = new Vector3(leftHand.position.x ,lowerPoint, leftHand.position.z);
					rightHand.position = new Vector3(rightHand.position.x ,upperPoint, rightHand.position.z);
				}
				else if(leftHand.position.y > upperPoint){
					leftHand.position = new Vector3(leftHand.position.x ,upperPoint, leftHand.position.z);
					rightHand.position = new Vector3(rightHand.position.x ,lowerPoint, rightHand.position.z);
				}
			}
			leftHand.Translate(0,Time.deltaTime * leftTranslate,0);
			rightHand.Translate(0,Time.deltaTime * rightTranslate,0);			
			motor.increaseSpeed(speedMultiplier);
		}
	}
	//function for increasing the rate at which the computer hand move up and down
	void increaseDifficulty(){
		int upOrDownL = (int)(leftTranslate / Mathf.Abs (leftTranslate));
		int upOrDownR = (int)(rightTranslate / Mathf.Abs (rightTranslate));
		if (motor.currentSpeed < SpeedtoIncreaseDifficulty [0]) {
			leftTranslate = 1 * upOrDownL;
			rightTranslate = 1 *upOrDownR; 
		}
		else if (motor.currentSpeed < SpeedtoIncreaseDifficulty [1]){
			speedChangeInterval = 2.0f;
			if(Time.time - speedChangeTimer > speedChangeInterval){
				speedChangeVary = Random.Range(1.5f, 2.5f);
				speedChangeTimer = Time.time;
			}
			leftTranslate = speedChangeVary * upOrDownL;
			rightTranslate = speedChangeVary *upOrDownR;
		}
		else if ( motor.currentSpeed < SpeedtoIncreaseDifficulty [2]) {
			speedChangeInterval = 2f;
			if(Time.time - speedChangeTimer > speedChangeInterval){
				speedChangeVary = Random.Range(2f, 4f);
				speedChangeTimer = Time.time;
			}
			leftTranslate = speedChangeVary * upOrDownL;
			rightTranslate = speedChangeVary *upOrDownR;
		}
		else {
			speedChangeInterval = 2.0f;
			if(Time.time - speedChangeTimer > speedChangeInterval){
				speedChangeVary = Random.Range(3.5f, 7f);
				speedChangeTimer = Time.time;
			}
			leftTranslate = speedChangeVary * upOrDownL;
			rightTranslate = speedChangeVary *upOrDownR;
		}

	}

	//function for showing a successful detection of player hands
	public void onPlayerHandDetected(bool handDetected){
		if (handDetected) {
			playerLeftHand.gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0, 1.0f);
			playerRightHand.gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0, 1.0f);
			playerLeftHand.gameObject.SetActive (handDetected);
			playerRightHand.gameObject.SetActive (handDetected);
		}
		else{
			playerLeftHand.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.0f, 0, 0.0f);
			playerRightHand.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.0f, 0, 0.0f);
		}

	}

	//function for checking if player hand is in same position as computer controlled hand
	bool checkedGestureAccuracyLeft(Transform handPos, Transform playerHandPos){
		if (playerHandPos.position.y < handPos.position.y + marginAllowed && playerHandPos.position.y > handPos.position.y- marginAllowed) {
			StartCoroutine(showSuccessLeft(0.1f));
			return true;
		}
		return false;
	}
	bool checkedGestureAccuracyRight(Transform handPos, Transform playerHandPos){
		if (playerHandPos.position.y < handPos.position.y+ marginAllowed && playerHandPos.position.y > handPos.position.y- marginAllowed){
			Debug.Log("yes");
			StartCoroutine(showSuccessRight(0.1f));
			return true;
		}
		return false;
	}
	//function to show a successful hit 
	IEnumerator showSuccessLeft(float timer){
		playerLeftHand.localScale += new Vector3 (0.01f, 0.01f, 0);
		yield return new WaitForSeconds(timer);
		playerLeftHand.localScale += new Vector3 (0.01f, 0.01f, 0);
		yield return new WaitForSeconds (timer);
		playerLeftHand.localScale += new Vector3 (0.01f, 0.01f, 0);
		yield return new WaitForSeconds (timer);
		playerLeftHand.localScale += new Vector3 (-0.01f, -0.01f, 0);
		yield return new WaitForSeconds(timer);
		playerLeftHand.localScale += new Vector3 (-0.01f, -0.01f, 0);
		yield return new WaitForSeconds (timer);
		playerLeftHand.localScale += new Vector3 (-0.01f, -0.01f, 0);
	}
	IEnumerator showSuccessRight(float timer){
		playerRightHand.localScale += new Vector3 (0.01f, 0.01f, 0);
		yield return new WaitForSeconds(timer);
		playerRightHand.localScale += new Vector3 (0.01f, 0.01f, 0);
		yield return new WaitForSeconds (timer);
		playerRightHand.localScale += new Vector3 (0.01f, 0.01f, 0);
		yield return new WaitForSeconds (timer);
		playerRightHand.localScale += new Vector3 (-0.01f, -0.01f, 0);
		yield return new WaitForSeconds(timer);
		playerRightHand.localScale += new Vector3 (-0.01f, -0.01f, 0);
		yield return new WaitForSeconds (timer);
		playerRightHand.localScale += new Vector3 (-0.01f, -0.01f, 0);
	}
}
