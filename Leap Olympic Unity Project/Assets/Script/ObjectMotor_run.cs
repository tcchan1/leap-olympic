﻿using UnityEngine;
using System.Collections;

public class ObjectMotor_run : MonoBehaviour {

	public float maxSpeed = 20.0f;
	public float minSpeed = 2.0f;
	public float currentSpeed = 0.0f;
	private float disSpeed = 0.001f;
	public float speedIncStep = 3.0f;
	float slownigTimer;
	// Use this for initialization
	void Start () {
		slownigTimer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(currentSpeed > 0) {
			// Time.deltaTime => translate per second
			transform.Translate(new Vector3(0, 0, currentSpeed* Time.deltaTime) );
			ReduceSpeed();
		}
	}

	public void increaseSpeed() {
		if (currentSpeed == 0)
			currentSpeed += minSpeed;
	}
	public void increaseSpeed(float multiplier){
		currentSpeed += speedIncStep * multiplier;
		if (currentSpeed > maxSpeed)
			currentSpeed = maxSpeed;
	}
	public void initSpeed() {
		currentSpeed = 0.0f;
	}
	void ReduceSpeed(){
		if(slownigTimer == 0)
			slownigTimer = Time.time;
		else if (Time.time - slownigTimer > 1.0f) {
			currentSpeed -= 1.0f;
			slownigTimer = Time.time;
			if (currentSpeed <= minSpeed ){
				currentSpeed = minSpeed;
				slownigTimer = 0;
			}
		}
	}
	public float GetCurrentSpeed(){
		return currentSpeed;
	}
}
