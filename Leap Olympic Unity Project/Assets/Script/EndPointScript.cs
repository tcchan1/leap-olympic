﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndPointScript : MonoBehaviour {

	public GameObject gameManager;
	private GameManagerScript gameManagerScript;

	public GameObject EndRecordText;

  	void Awake() {
		gameManagerScript = gameManager.GetComponent<GameManagerScript> ();
	}

	void OnTriggerEnter(Collider other) {
		if(other.name == "PlayerTrigger") 
			gameManagerScript.playerReachEndPoint();		
	}

	public void OnGameEnd() {
	}
}
