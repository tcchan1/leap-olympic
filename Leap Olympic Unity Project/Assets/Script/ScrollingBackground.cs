﻿using UnityEngine;
using System.Collections;

public class ScrollingBackground : MonoBehaviour {
	public Transform player;
	public float offsetZ;
	void Update () {
		if (player.position.z > transform.position.z + offsetZ/3)
			transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z + offsetZ);
	}
}
