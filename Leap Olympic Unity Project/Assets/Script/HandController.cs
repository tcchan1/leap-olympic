﻿using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {

	private float initYPos;
	public float leapToScreenRatio;
	[SerializeField] float topBoundary = 2.4f, botBoundary = -0.24f;
	GameControl gameControlScript;
	void Awake(){
		gameControlScript = GameObject.Find ("GameManager").GetComponent<GameControl>();

	}
	void Start() {
		this.initYPos = transform.position.y;
	}
	public void goTop(){
		transform.position = new Vector3 (transform.position.x, topBoundary,transform.position.z );
	}
	public void goBot(){
		transform.position = new Vector3 (transform.position.x, botBoundary,transform.position.z );
	}
	/*public  void doMovement(float yDistMoved){
		transform.position = new Vector3 (transform.position.x, transform.position.y+yDistMoved * (1/leapToScreenRatio),transform.position.z );
		checkBoundary ();
	}
	void checkBoundary(){
		if (transform.position.y > topBoundary)
			transform.position = new Vector3 (transform.position.x, topBoundary, transform.position.z);
		else if (transform.position.y < botBoundary)
			transform.position = new Vector3 (transform.position.x, botBoundary, transform.position.z);
	}*/
	public void InitPos() {
		transform.position = new Vector3 (transform.position.x, initYPos, transform.position.z);
		gameControlScript.onPlayerHandDetected(false);
	}
}
