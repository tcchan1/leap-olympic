﻿using UnityEngine;
using System.Collections;
using Leap;
using System;
using System.IO;



public class Leap_manager : MonoBehaviour {
	//in-game object
	public GameObject cubeObj;
	public GameObject leftHandObj = null;
	public GameObject rightHandObj = null;
	public GameObject arrowObj = null;
	public GameObject cameraObj = null;

	// get the game manager to make condition and finish the game flow
	public GameObject gameManager = null;
	private GameManagerScript gameManagerScript;
	private GameControl gameControlScript;

	//variables to determine which status to play
	public bool running;
	public bool swimming;
	public bool shooting;
	public bool menu;

	private ObjectMotor_run motor;
	//variables for the leap controller
	private Leap.Controller leapController = null;
	private Leap.Hand leapHand = null;
	private bool leapInit = false;

	//variables for the leap frame
	private Leap.Frame leapFrame = null;
	private Leap.Frame frame;
	//what is going on ???
	private Int64 lastFrameID = 0;
	private Int64 leapFrameCounter = 0;
	
	private bool leftHandState = false;
	private bool rightHandState = false;

	// testing shooting
	GameObject aimPic;
	GameObject arrow;
	Vector2 aim = new Vector2 (0, 0);
	Vector2 originPos = Vector2.zero;
	bool hasShot;
	float delayer = 0;


	//running parameter, min value for movement detection, player need to move at least this value to have effect on screen, to prevent too much movement 
	public float posDeviation = 3.0f;

	// Use this for initialization
	void Start () {
		//script for controlling movement of player object
		motor = cubeObj.GetComponent<ObjectMotor_run> ();
		aimPic = GameObject.Find ("New Sprite");
		arrow = GameObject.Find ("Arrow");
		hasShot = false;

		// script from game manager
		gameManagerScript = gameManager.GetComponent<GameManagerScript> ();
		gameControlScript = gameManager.GetComponent<GameControl> ();

		//initialize leap controller + enable gesture
		leapController = new Leap.Controller();
		leapController.EnableGesture(Gesture.GestureType.TYPECIRCLE);
		leapController.EnableGesture (Gesture.GestureType.TYPESWIPE);
		//set the parameters of the gestures
		setSwipeParameters ();
		leapController.Config.Save();
		leapInit = true;

		// Pass the player data to game manager
		gameManagerScript.SetPlayerInitPos (new Vector3(0f, 1f, -340));
	}
	
	// Update is called once per frame
	void Update () {
		//read frame of the controller, update game according to information within a leap frame + error checking
		if(leapInit && leapController != null) {
			frame = leapController.Frame();
			if(frame.IsValid && (frame.Id != lastFrameID)) {
				if(gameManagerScript.isGameStart()) {
					leapFrame = frame;
					lastFrameID = frame.Id;
					leapFrameCounter++;
					if(running) 
						DoRunning();
					if(swimming)
						DoSwimming();
					if(shooting){// && leftHandObj != null && rightHandObj != null)
						DoShooting();
					}
					if(menu) {
						DoControlling();
					}
					if(leftHandObj != null && rightHandObj != null) {
						// transform the hand object (refer to leap motion hands information)
					}
				} else {
					if(running) {
						DoRunningReady();
					}
				}
			}//end if
		}//end if 
	}
	//function for the swimming game
	void DoSwimming(){
		GestureList g_list = frame.Gestures();
		for(int i = 0 ; i < g_list.Count; i++) {
			Gesture gesture  = g_list[i];
			switch(gesture.Type) {
				/* this part is doing swipe as swimming at leap motion*/
			case Gesture.GestureType.TYPESWIPE:
				SwipeGesture swipeGesture = new SwipeGesture(gesture);
				HandList handList = frame.Hands;

				// try to get the previous frame to make condition
				Leap.Frame previousFrame = leapController.Frame (1);

				//must be able to detect both hand
				//if(handList.Count > 1){
				//	Debug.Log ("detecting...");
				//	if(handList.Rightmost.Equals(swipeGesture.Hands[0]))
				//		rightHandState = true;
				//	if(handList.Leftmost.Equals(swipeGesture.Hands[0]))
				//		leftHandState = true;
				//}
				// ^^ it may be too hard to control the character to move forward

				// using guestures direction and which hand to identify move or not
				float xDirection = swipeGesture.Direction.x; // If x > 0, direction = right; If x < 0, direction = left.

				//Debug.Log ("Current: " + frame.Hands[0].Id + "; Pre: " + previousFrame.Hands[0].Id);
				//Debug.Log(handList.Rightmost.PalmPosition);
				//
				if(xDirection > 0 && handList[0].PalmPosition.x > 0) {
					rightHandState = true;
				}
				if(xDirection < 0 && handList[0].PalmPosition.x < 0) {
					leftHandState = true;
				}
				// ^^ there are have a problem for using this method to implement. Player can use one hand to play.

				if(rightHandState && leftHandState) {
					//cubeObj.gameObject.transform.Translate(new Vector3(0, 0, 1));
					motor.increaseSpeed();
					leftHandState = false;
					rightHandState = false;
				}

				break;
				
			}//end switch
		}//end for

	}//end DoSwimming
	//set the parameter of the swipe gesture
	void setSwipeParameters(){
		leapController.Config.SetFloat("Gesture.Swipe.MinLength", 200.0f);
		leapController.Config.SetFloat("Gesture.Swipe.MinVelocity", 750f);
	}
	//function for the running game
	void DoRunning(){
		HandList handList = frame.Hands;
		Hand leftHand = handList.Leftmost;
		Hand rightHand = handList.Rightmost;
		FingerList fingers = frame.Fingers;
		
		// condition for leap motion that there are 2 hands which are left hand or right hand
		if(leftHand != null && rightHand != null && handList.Count >= 2 && fingers.Count <= 3) {
			gameControlScript.onPlayerHandDetected(true);
			controlHandModel(leftHand.PalmPosition.y, rightHand.PalmPosition.y);
		} else {
			InitPlayerHandsPosY();
		}
	}//end DoRunning
	void controlHandModel(float leftPosY, float rightPosY){
		if(leftHandObj != null && rightHandObj != null) {
			// transform the hand object (refer to leap motion hands information)
			if(leftPosY > rightPosY + posDeviation){
				leftHandObj.GetComponent<HandController>().goTop();
				rightHandObj.GetComponent<HandController>().goBot();
			}
			else if(rightPosY > leftPosY + posDeviation){
				leftHandObj.GetComponent<HandController>().goBot();
				rightHandObj.GetComponent<HandController>().goTop();
			} 
		}	
	}
	void InitPlayerHandsPosY() {
		leftHandObj.GetComponent<HandController> ().InitPos ();
		rightHandObj.GetComponent<HandController> ().InitPos ();
	}
	//function for shooting 
	void DoShooting(){
		HandList hands = frame.Hands;
		Hand left = hands.Leftmost;
		Hand right = hands.Rightmost;
		float dis = 0;
		FingerList fingers = frame.Fingers;
		if(hasShot)
			arrow.transform.LookAt(arrow.transform.position + arrow.rigidbody.velocity);
		//if both fist detected, calculate the distance between the 2 fists
		if (left != null && right != null && hands.Count > 1) {
			delayer = 0;
			dis = Math.Abs(left.PalmPosition.z - right.PalmPosition.z);
			//if 2 fists are a 100 units apart (for the first time), record the position of the front fist
			if(originPos == Vector2.zero && dis > 100){
				originPos.x = hands.Frontmost.PalmPosition.x;
				originPos.y = hands.Frontmost.PalmPosition.y;
			}
			//if 2 fists are 100 units apart (past position exist), translate the aim accoriding to the movement of the front fist
			else if(originPos != Vector2.zero && dis > 100){
				aim.x = hands.Frontmost.PalmPosition.x - originPos.x;
				aim.y = hands.Frontmost.PalmPosition.y - originPos.y;
				aimPic.transform.position = new Vector3(aim.x/10, aim.y/10,-5);
				if(!hasShot)
					arrow.transform.LookAt(new Vector3( aim.x/20, aim.y/20, 0 ));
				//Debug.Log("Aiming at " + aimPic.transform.position.x + " & " +aimPic.transform.position.y + " with power : " + dis);
			}

		}//end if (detecting 2 hands)
		//if 
		if (fingers.Count >= 5 && dis > 50 && !hasShot){
			//calculate the difference in position of camera and aim
			Vector3 aimDir = new Vector3(aimPic.transform.position.x *150, aimPic.transform.position.y*150 ,dis*5);
			//Debug.Log("shoot at direction " + aimDir);
			arrow.rigidbody.useGravity = true;
			arrow.rigidbody.AddForce(aimDir);
			hasShot = true;
		}
		else{
			if(delayer == 0)
				delayer = Time.time;
			else{
				if(Time.time - delayer > 1){
					aimPic.transform.position = new Vector3(0,0,0);
					delayer = 0;
					originPos = Vector2.zero;
				}
			}
		}//end else (not able to detect two hands)
	}

	void DoControlling() {

	}

	void DoRunningReady() {
		HandList hands = frame.Hands;
		FingerList left_fingers = hands.Leftmost.Fingers;
		FingerList right_fingers = hands.Rightmost.Fingers;
		//Debug.Log ("Hands Count: " + hands.Count + ", L_Fingers Count: " + left_fingers.Count + ", R_Fingers Count: " + right_fingers.Count);
		if(hands.Count >= 2) {
			gameControlScript.onPlayerHandDetected(true);
			gameManagerScript.SetIsCountDown(true);
			if(left_fingers.Count <= 2 && right_fingers.Count <= 2) {
				if(gameManagerScript.getReadyState()) {
					gameManagerScript.setGameStart(true);
					motor.initSpeed();
				}
			}
		} else {
			gameManagerScript.SetIsCountDown(false);
			gameManagerScript.ResetCountDown();
			gameControlScript.onPlayerHandDetected(false);
		}
	}
}
