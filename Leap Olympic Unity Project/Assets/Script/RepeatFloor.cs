﻿using UnityEngine;
using System.Collections;

public class RepeatFloor : MonoBehaviour {
	[SerializeField] Transform player;
	[SerializeField] float offset;
	[SerializeField] float posDisplacement;
	[SerializeField] Vector3 orgPos;
	// Update is called once per frame
	void Awake(){
		orgPos = transform.position;
	}
	void Update () {
		if(player.position.z > transform.position.z + offset)
			transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z + posDisplacement);
	}
	public void resetPos(){
		transform.position = orgPos;
	}
}
